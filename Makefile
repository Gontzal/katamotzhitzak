VALAC = valac
VFLAG += -X -O4 -X '-lSDL2_image' -X '-lSDL2_mixer' -X '-lSDL2_ttf' -X '-lSDL2_gfx' -X '-lSDL2'  -X '-lSDL2' 
VPKGS += --pkg sdl2 --pkg sdl2-image --pkg sdl2-mixer --pkg sdl2-ttf --pkg sdl2-gfx --pkg gee-0.8 --pkg posix
VBASE +=  $(PROYECTO_DIR)/*.gs
VBASE +=  $(PROYECTO_DIR)/../sdlk/*.gs 

ifeq ($(CROSS), ANDROID)
	ANDROID_TOOLCHAIN ?= /opt/android-toolchain/
	ANDROID_PREFIX ?= arm-linux-androideabi
	
	PKG_CONFIG_PATH = "$(ANDROID_TOOLCHAIN)/sysroot/usr/lib/pkgconfig"
	LDFLAGS = '-Wl,--fix-cortex-a8' 
	CFLAGS = '-march=armv7-a -mfloat-abi=softfp -mfpu=neon' 
	
	VFLAG := --cc=$(ANDROID_TOOLCHAIN)/bin/$(ANDROID_PREFIX)-gcc \
		-X -L"$(ANDROID_TOOLCHAIN)/$(ANDROID_PREFIX)/lib" \
		-X -L"$(ANDROID_TOOLCHAIN)/sysroot/usr/lib" \
		-X -I"$(ANDROID_TOOLCHAIN)/sysroot/usr/include" \
		-X '-Wl,-Bstatic' -X '-lffi' -X '-lintl' -X '-liconv' $(VFLAG) \
		-X '-lgio-2.0' -X '-lgee-0.8' -X '-lglib-2.0' \
		-X '-Wl,-Bdynamic' -X '-llog' -X '-lc' -X '-lm' -X '-lz' -X '-ldl' -X '-lGLESv1_CM' -X '-lGLESv2' -X '-landroid' -X '-lstdc++' --pkg sdl2-android
		
	# ????         \ --pkg sdl2-android       ???? it gives me error. Why?
	
	# ANDROID USES SDL JNI CALLS
	JNI = 0
	
	VBASE += $(PROYECTO_DIR)/miprograma_android_main.c
	SHARED = 1
	OS = Android
endif


ifeq ($(DEBUG), 1)
	VFLAG += --enable-gobject-tracing --enable-checking -g
endif


ifeq ($(SHARED), 1)
	VFLAG+= -X -shared
	ifeq ($(OS), Android)
		BIN =  ./libs/armeabi/libgame-android.so
	endif
endif


.PHONY:
	 rmdir "./bin"
	$(CLEAN)

build:
	@mkdir "./bin" 2>/dev/null; true;
	$(VALAC) --disable-warnings --enable-deprecated $(VBASE) $(VPKGS) $(VFLAG) -o $(BIN)

clean: .PHONY
default: build

