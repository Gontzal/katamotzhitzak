/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */


class Datos:Object // capa de datos
	// variables necesarias para uso de listas configuradas para cada alumno.
	todas_palabras: list of string
	todas_letras: list of string
	todas_vocales: list of string
	todas_consonantes: list of string
	todas_fonemas: list of string
	todas_triples:list of string
	todas_dobles:list of string
	todas_simples:list of string
	
	alumno_fonemas:list of string
	alumno_letras:list of string
	alumno_triples:list of string
	alumno_dobles:list of string
	alumno_simples:list of string
	
	todas_silabas:list of string
	todas_silabas_directas:list of string
	alumno_palabras:list of string
	alumno_idioma:string

	todas_frases:list of string
	
	init
		todas_triples=new list of string
		todas_dobles= new list of string
		todas_simples=new list of string
		
		alumno_triples=new list of string
		alumno_dobles=new list of string
		alumno_simples=new list of string
		
		todas_fonemas=new list of string
		todas_letras=new list of string
		todas_vocales=new list of string
		todas_consonantes=new list of string
		todas_palabras= new list of string
		
		alumno_palabras= new list of string
		alumno_letras= new list of string
		todas_silabas= new list of string
		todas_silabas_directas= new list of string
		
		todas_frases=new list of string

	def toma_alumno_letra ():string
		return datos.alumno_letras[Random.int_range(0,tamano_de_lista(datos.alumno_letras))]
	
	def toma_alumno_letra_simple ():string
		return datos.alumno_simples[Random.int_range(0,tamano_de_lista(datos.alumno_simples))]
	
	def toma_alumno_letra_doble ():string
		return datos.alumno_dobles[Random.int_range(0,tamano_de_lista(datos.alumno_dobles))]
	
	def toma_letra_total ():string
		return datos.todas_letras[Random.int_range(0,tamano_de_lista(datos.todas_letras))]

	def toma_letra_total_simple ():string
		return datos.todas_simples[Random.int_range(0,tamano_de_lista(datos.todas_simples))]

	def toma_palabra_alumno ():string
		return datos.alumno_palabras[Random.int_range(0,tamano_de_lista(datos.alumno_palabras))]

	def toma_palabra_total ():string
		return datos.todas_palabras[Random.int_range(0,tamano_de_lista(datos.todas_palabras))]

	def toma_frase_total ():string
		return datos.todas_frases[Random.int_range(0,tamano_de_lista(datos.todas_frases))]

	def toma_alumno_letras(num:int):list of string
		letras:list of string= new list of string
		for var i=0 to (num)
			var l= this.toma_alumno_letra()
			while (letras.contains(l)) 
				l = this.toma_alumno_letra()
			letras.add(l)
		return letras

	def transforma_archivo_str_frase(frase:string):string
		return (todas_frases.index_of(frase)+1).to_string()

	def transforma_archivo_str(palabra:string):string
		return tostring(4,todas_palabras.index_of(palabra))

	def transforma_archivo_int(palabra:string):int
		return todas_palabras.index_of(palabra)
	
	def toma_palabras_alumno(num:int):list of string  // toma "num" palabras de la lista de palabras de alumno
		palabras:list of string= new list of string
		for var i=0 to (num)
			var l= this.toma_palabra_alumno()
			while (palabras.contains(l)) 
				l = this.toma_palabra_alumno()
			palabras.add(l)
		return palabras
	
	def toma_palabras_total(num:int):list of string  // toma "num" palabras de la lista palabras total
		palabras:list of string=new list of string
		for var i=0 to (num)
			var l= this.toma_palabra_total()
			while (palabras.contains(l))
				l = this.toma_palabra_total()
			palabras.add(l)
		return palabras
	
	def elegir_elemento_de_lista (lista:list of string): string
		return lista[Random.int_range(0,tamano_de_lista(lista))]
		
		
	def crea_lista_de_letras_desde_palabra(s:string): list of string
		letras:list of string= new list of string
		for var i=0 to ultima(s)
			letras.add(toma_letra(s,i))
		return letras
		
	def crea_lista_de_sonidos_desde_palabra(s:string): list of string
		letras:list of string= new list of string
		var cont=0
		
		while cont <=ultima(s)
			//triples
			var triple=toma_cadena(s,cont,cont+3)
			if (triple!="") and (todas_triples.contains(triple)) 
				cont+=3
				letras.add(triple)
			else
				//dobles
				var doble=toma_cadena(s,cont,cont+2)
				if (doble!="") and (todas_dobles.contains(doble))
					cont+=2
					letras.add(doble)
				else
					//simples restantes
					var simple=toma_cadena(s,cont,cont+1)
					cont+=1
					letras.add(simple)
			
		return letras
		
				
				
	
				
	def abriendo_archivos_necesarios(nombre:string, contrasena:string):bool
		var r=false
		
		return r
		
		
		
		

	def toma_una_palabra_que_empiece_con_sonido (s:string, lista:list of string ) : string
		var lista_sonidos= new list of string
		var nuevas_palabras= new list of string
		for var i=0 to ultimo_de_lista(lista)
			lista_sonidos.clear()
			lista_sonidos = crea_lista_de_sonidos_desde_palabra(lista[i])
			
			if toma_cadena(lista[i],0,longitud(s))==s
				nuevas_palabras.add(lista[i])
		var r=""
		if not lista_vacia(nuevas_palabras)
			r= nuevas_palabras[Random.int_range(0,tamano_de_lista(nuevas_palabras))]
		return r 
	
	def toma_una_palabra_que_empiece_con_letra(s:string,lista:list of string): string
		
		var nuevas_palabras= new list of string
		for var i=0 to ultimo_de_lista(lista)
			if toma_cadena(lista[i],0,longitud(s))==s
				nuevas_palabras.add(lista[i])
		var r=""
		if not lista_vacia(nuevas_palabras)
			r= nuevas_palabras[Random.int_range(0,tamano_de_lista(nuevas_palabras))]
		
		return r 
				
	def toma_una_palabra_que_tenga_sonido (s:string,lista:list of string): string
		var nuevas_palabras= new list of string
		var lista_sonidos= new list of string
		for var i=0 to ultimo_de_lista(lista)
			lista_sonidos.clear()
			lista_sonidos = crea_lista_de_sonidos_desde_palabra(lista[i])
			if lista_sonidos.contains(s)
				nuevas_palabras.add(lista[i])
				
		var r=""
		if not lista_vacia(nuevas_palabras)
			r= nuevas_palabras[Random.int_range(0,tamano_de_lista(nuevas_palabras))]
		return r 
	
	def toma_una_palabra_que_tenga_letra (s:string,lista:list of string): string
		var nuevas_palabras= new list of string
		var lista_sonidos= new list of string
		for var i=0 to ultimo_de_lista(lista)
			lista_sonidos.clear()
			lista_sonidos = crea_lista_de_letras_desde_palabra(lista[i])
			if lista_sonidos.contains(s)
				nuevas_palabras.add(lista[i])
				
		var r=""
		if not lista_vacia(nuevas_palabras)
			r= nuevas_palabras[Random.int_range(0,tamano_de_lista(nuevas_palabras))]
		return r 
	
	def divide_palabra_en_silabas(s:string):list of string
		var r= new list of string
		var arraystr= s.split ("_")
		convertir_array_en_lista(arraystr,ref r )
		return r

	def divide_frase_en_palabras(s:string):list of string
		var r= new list of string
		var arraystr= s.split (" ")
		convertir_array_en_lista(arraystr,ref r )
		return r

	def crea_lista_de_silabas(n:int):list of string
		var r= new list of string
		for var i=1 to n
			r.add(todas_silabas[Random.int_range(0,tamano_de_lista(todas_silabas))])
		return r
	
	def crea_lista_de_silabas_directas_sin_repeticiones(n:int):list of string
		var r= new list of string
		for var i=1 to n
			var encontrado=false
			var vocales="aeiou"
			while encontrado==false
				var letra=toma_alumno_letra()
				var silaba=letra+toma_cadena (vocales,Random.int_range(0,4))
				if (todas_silabas.contains(silaba)) and (not r.contains(silaba))
					r.add(silaba)
					encontrado=true
		return r
	def crea_lista_de_silabas_directas_sin_repeticiones_con_letra(n:int,vocal:string):list of string
		var r= new list of string
		for var i=1 to n
			var encontrado=false
			while encontrado==false
				var letra=toma_alumno_letra()
				var silaba=letra+vocal
				if (todas_silabas.contains(silaba)) and (not r.contains(silaba))
					r.add(silaba)
					encontrado=true
		return r

	def crea_lista_de_silabas_directas_con_letra(n:int,vocal:string):list of string
		var r= new list of string
		for var i=1 to n
			var encontrado=false
			while encontrado==false
				var letra=toma_alumno_letra()
				var silaba=letra+vocal
				if (todas_silabas.contains(silaba))
					r.add(silaba)
					encontrado=true
		return r

	def crea_lista_de_silabas_directas_con_letra_diferentes(n:int,vocal:string):list of string
		var r= new list of string
		for var i=1 to n
			var encontrado=false
			while encontrado==false
				var letra=toma_alumno_letra()
				var silaba=letra+vocal
				if (todas_silabas.contains(silaba)) and (not r.contains(silaba))
					r.add(silaba)
					encontrado=true
		return r

	def crea_lista_de_palabras_segun_medida ( m1:int, m2:int, lista_principal:list of string ):list of string
		var lista= new list of string
		for var i=0 to ultimo_de_lista(lista_principal)
		
			if longitud (lista_principal[i])>=m1 and longitud (lista_principal[i])<=m2
				lista.add(lista_principal[i])
		return lista
	def crea_lista_de_silabas_triples_diferentes(n:int) :list of string
		var r= new list of string
		for var i=1 to n
			var encontrado=false
			while encontrado==false
				var num= Random.int_range(0,tamano_de_lista(datos.todas_silabas))
				var silaba=datos.todas_silabas[num]
				if (not r.contains(silaba)) and (longitud(silaba)==3)
					r.add(silaba)
					encontrado=true
		return r
		
	
