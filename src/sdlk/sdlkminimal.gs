/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
// version 2.0 de sdlk para SDL2.0

uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

class Fuentes: Object
	fuente: Font
		

class SdlkMinimal:Object
	Ancho:int16
	Alto:int16 
	lista_general: list of Control 
	lista_profundidad: list of int 
	pantalla:SDL.Window 
	Referencia_general:int=0 
	Focus:int=-1 
	Puntero_x:int 
	Puntero_y:int
	Rec:SDL.Rect
	musica2:SDLMixer.Music 
	lista_fuentes: dict of int,Fuentes 
	Teclas: dict of string,bool 
	event tecleado(s:string) 
	event tecleado_up(s:string) 
	event update()
	
	init
		lista_fuentes= new dict of int,Fuentes
		Teclas= new dict of string,bool
		lista_profundidad= new list of int
		lista_general=new list of Control
		
	construct (ancho:int16,alto:int16)
		Ancho=ancho
		Alto=alto
	
	def cargafuentekatamotz2(tamano:int):unowned Font
		fuente: unowned Font
		if not lista_fuentes.has_key(tamano)
			var mifuente= new Fuentes
			mifuente.fuente= (new Font(directorio_datos+"KatamotzIkasi.ttf",tamano))
			lista_fuentes[tamano]=mifuente
			fuente = lista_fuentes[tamano].fuente
		else
			fuente = lista_fuentes[tamano].fuente
		return fuente

	def get_transparente (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		alpha:uchar=0
		
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		
		case img.format.BytesPerPixel
			when 1,2,3
				alpha=255
				
			when 4
				dire+=+3
				alpha=*dire
		img.unlock()
		return alpha

	def get_R (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		
		img.do_lock()
		dire=img.pixels
		dire+= (y*img.pitch)+ x*(img.format.BytesPerPixel)
		color=*dire
		img.unlock()
		return color
		
	def get_G (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=1
		color=*dire
		img.unlock()
		return color
	
	def get_B (x:int,y:int,img:SDL.Surface): uchar
		dire:uint8*
		color:uchar=0
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=2
		color=*dire
		img.unlock()
		return color
		
	def set_R (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		*dire=color
		img.unlock()
		
	def set_G (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=1
		*dire=color
		img.unlock()
		
		
	def set_B (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=2
		*dire=color
		img.unlock()
		
		
	def set_transparente (x:int,y:int,ref img:SDL.Surface,color:uchar)
		dire:uint8*
		img.do_lock()
		dire=img.pixels 
		dire+= (y*img.pitch)+ (x*img.format.BytesPerPixel)
		dire+=+3
		*dire=color
		img.unlock()
	
	def abrillanta_imagen(ref img:SDL.Surface)
		
		var dif = 60 // el grado de CLARIDAD
		for var i=0 to ((img.h-1))
			for var j=0 to ((img.w-1))
				var R=(int)get_R(j,i,img)
				var G=(int)get_G(j,i,img)
				var B=(int)get_B(j,i,img)
				if R+dif <= 255
					set_R(j,i,ref img,(uchar)R+dif )
				else
					set_R(j,i,ref img,255 )

				if G+dif <= 255
					set_G(j,i,ref img,(uchar)G+dif )
				else
					set_G(j,i,ref img,255 )
					
				if B+dif <= 255
					set_B(j,i,ref img,(uchar)B+dif )
				else
					set_B(j,i,ref img,255 )
				
	def colorea_imagen(ref img:SDL.Surface,r:int,g:int,b:int)
		
		for var i=0 to ((img.h-1))
			for var j=0 to ((img.w-1))
				var A=(int)get_transparente(j,i,img)
				if A > 20
					set_R(j,i,ref img,(uchar)r )
					set_G(j,i,ref img,(uchar)g )
					set_B(j,i,ref img,(uchar)b )
				
	// elimina el formato <12345> de un string
	def sin_formato(s:string):string
		var r=""
		///eliminar las <> que existen en el texto creando un texto crudo.
		var i=0
		while i<=ultima(s)
			var letra=toma_cadena(s,i)
			if letra=="<" 
				i+=7
			else
				r+=letra
				i++
		return r
	
	def cargafuente(tamano:int):Font
		var fuente=new Font(directorio_datos+"KatamotzIkasi.ttf",tamano)
		return fuente 
		
	def genera_elemento(este:Control)
		// busca un lugar vacio
		var lugar=-1
		for var i=0 to ultimo_de_lista(lista_general)
			if not lista_profundidad.contains(i)
				lugar=i
				break
		
		if lugar==-1 //si el lugar esta vacio pon el ultimo
			este.Index=sdlk.Referencia_general
			sdlk.lista_general.add(este)
			sdlk.lista_profundidad.add(este.Index)
			sdlk.Referencia_general++
		else // si no hay mas anade al final
			este.Index=lugar
			sdlk.lista_profundidad.add(lugar)
			sdlk.lista_general.remove_at(lugar)
			sdlk.lista_general.insert(lugar,este)
			

	

	//revisa las colisiones entre dos imagenes 
	def colision_cuadrada(a:Control,b:Control):bool
		var res=false
		if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
			/*a - bottom right co-ordinates*/
			var ax=a.dr.x
			var ay=a.dr.y
			var ax1 = a.dr.x + a.dr.w - 1;
			var ay1 = a.dr.y + a.dr.h - 1;
			
			/*b - bottom right co-ordinates*/
			var bx=b.dr.x
			var by=b.dr.y
			var bx1 = b.dr.x + b.dr.w - 1;
			var by1 = b.dr.y + b.dr.h - 1;

			/*check if bounding boxes intersect*/
			var rectcolision=true
			if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
			if((by1 < ay) or (ay1 < by)) do rectcolision=false
			res=rectcolision
			
		return res
		
		
	
	def colision_mascara(a:Control,b:Control):bool
		//no funciona si las imagenes han sido ampliadas o reducidas.
		var res=false
		if (a.Index!=b.Index) and (a.Visible) and (b.Visible)
			/*a - bottom right co-ordinates*/
			var ax=a.dr.x
			var ay=a.dr.y
			var ax1 = a.dr.x + a.dr.w - 1;
			var ay1 = a.dr.y + a.dr.h - 1;
			
			/*b - bottom right co-ordinates*/
			var bx=b.dr.x
			var by=b.dr.y
			var bx1 = b.dr.x + b.dr.w - 1;
			var by1 = b.dr.y + b.dr.h - 1;

			/*check if bounding boxes intersect*/
			var rectcolision=true
			if((bx1 < ax) or (ax1 < bx)) do rectcolision=false
			if((by1 < ay) or (ay1 < by)) do rectcolision=false
			
			if rectcolision
				if (a.Nombre=="Imagen") and (b.Nombre=="Imagen")
					//recoge el cuadrado de interseccion
					var x0 = int.max(ax,bx)
					var y0 = int.max(ay,by)
					var x1 = int.min(ax1,bx1)
					var y1 = int.min(ay1,by1)
					
					var aw=a.dr.w 
					var bw=b.dr.w 
					res=false
					
					
					//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
					for var j=y0 to (y1-1)
						for var i=x0 to (x1-1)
							if ( a.Mascara[(i-ax)  +(j-ay)* aw  ] != ' ' )  and  ( b.Mascara[ (i-bx)+(j-by)*bw ] != ' ' )
								res=true
								break
				else
					if (a.Nombre=="Imagen") 
						//recoge el cuadrado de interseccion
						var x0 = int.max(ax,bx)
						var y0 = int.max(ay,by)
						var x1 = int.min(ax1,bx1)
						var y1 = int.min(ay1,by1)
						
						var aw=a.dr.w 
						res=false
						//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
						for var j=y0 to (y1-1)
							for var i=x0 to (x1-1)
								////print"imprime -%c-",a.Mascara[(i-ax)  +(j-ay)* aw  ]
								if ( a.Mascara[(i-ax)  +(j-ay)* aw  ] != ' ' ) 
									res=true
									break
					else	
						if (b.Nombre=="Imagen") 
							//recoge el cuadrado de interseccion
							var x0 = int.max(ax,bx)
							var y0 = int.max(ay,by)
							var x1 = int.min(ax1,bx1)
							var y1 = int.min(ay1,by1)
							
							var bw=b.dr.w 
							res=false
							//recorre el cuadrado de intersección usando la mascara la mascara tiene la coordenada X reducida en 8.
							for var j=y0 to (y1-1)
								for var i=x0 to (x1-1)
									////print"imprime -%c-",a.Mascara[(i-ax)  +(j-ay)* aw  ]
									if ( b.Mascara[(i-bx)  +(j-by)* bw  ] != ' ' ) 
										res=true
										break
						else
							res=true
					
				
		return res

	def convertir_surface_en_mascara (img:SDL.Surface):list of uchar
		str:list of uchar
		str = new list of uchar
		str.clear()
		lista: list of uchar
		lista=new list of uchar
		////print"12345678901234567890"
		for var j=0 to (img.h-1)
			for var i=0 to (img.w-1)
				if sdlk.get_transparente(i,j,img)>50
					str.add('x')
				else
					str.add(' ')				
		return str

	def pintar()
		var c=0
		while c<=ultimo_de_lista(lista_profundidad)
			if lista_profundidad[c]!=sdlk.Focus do this.lista_general[ lista_profundidad[c] ].pinta()
			c++
		if (sdlk.Focus>-1) do this.lista_general[ sdlk.Focus ].pinta()
			
		screen.present();
		window.update_surface ();
	
		
	def Rectangulo (screen:Renderer,dr:Rect,R:uchar,G:uchar,B:uchar,A:uchar)
		screen.set_draw_color(R, G, B, A);
		screen.fill_rect( {(int16)dr.x, (int16)dr.y, (int16)dr.w, (int16)dr.h} ) ;
		
		
	def Boton_redondo(screen:Renderer,dr:Rect,R:uchar,G:uchar,B:uchar,A:uchar)
		screen.set_draw_color(R, G, B, A);
		screen.fill_rect( {(int16)dr.x, (int16)dr.y, (int16)dr.w, (int16)dr.h} ) ;
	
	def Rectangulo_xywh (screen:Renderer, x:int, y:int, w:int, h:int, R:uchar, G:uchar, B:uchar, A:uchar)
		screen.set_draw_color(R, G, B, A)
		screen.fill_rect( { (int16)x, (int16)y, (int16)w, (int16)h} ) 
	
		

	def colision_n (a:int,b:int):bool
		var oa= lista_general[a].dr
		var ob= lista_general[b].dr
		var exit=false
		if (lista_general[a].Visible) and (lista_general[b].Visible) and (a!=b)
			var ax=oa.x
			var ay=oa.y
			var ax1 = oa.x + oa.w - 1;
			var ay1 = oa.y + oa.h - 1;
			
			var bx=ob.x
			var by=ob.y
			var bx1 = ob.x + ob.w - 1;
			var by1 = ob.y + ob.h - 1;

			/*check interseccion*/
			exit=true
			if((bx1 < ax) or (ax1 < bx)) do exit=false
			if((by1 < ay) or (ay1 < by)) do exit=false
		
		return exit
		
	def colision_xy (x:int,y:int,c:Control):bool
		var r=true
		if (x<c.dr.x ) or (x > c.dr.x+c.dr.w ) do r=false
		if (y<c.dr.y ) or (y > c.dr.y+c.dr.h ) do r=false
		return r
		
	def muere_todo()
		var contr= ultimo_de_lista(lista_profundidad)
		while contr>=0
			var c=lista_general[lista_profundidad[contr]]
			c.muere()
			contr--
		
		
	def update_control()
		sdlk.update()
		var contr= ultimo_de_lista(lista_profundidad)
		while contr>=0
			var c=lista_general[lista_profundidad[contr]]
			c.update(c)
			contr--
	
	def mira_sobre()// mira que se ponga sobre las cosas
		if ultimo_de_lista(lista_profundidad)>=0
			var ctr= ultimo_de_lista(lista_profundidad)
			while ctr>=0
				var c=lista_general[lista_profundidad[ctr]]
				
				if (sdlk.colision_xy(Puntero_x,Puntero_y,c)) and (c.Visible)
					c.sobre(c)
					break
				ctr--
	
	def get_Ancho_Pantalla():int
		return (int)(ancho/rw)
	
	def get_Alto_Pantalla():int
		return (int)(alto/rh)
		
	def toma_eventos(eventos:Event)
		if ultimo_de_lista(lista_profundidad)>=0
			case eventos.type
				when EventType.WINDOWEVENT
					if (eventos.window.event  == WindowEventID.RESIZED)
						var w=0; var h=0
						window.get_size(out w, out h); 
						sdlk.Alto  = h
						sdlk.Ancho =  w
						rw=(double)((double)sdlk.Ancho/1366.0)
						
					if (eventos.window.event  == WindowEventID.FOCUS_LOST)
						ev:Event
						while true
							Event.wait(out ev)
							if (ev.window.event== WindowEventID.FOCUS_GAINED)
								break
										
				when EventType.MOUSEBUTTONUP
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					if sdlk.Focus!=-1
						var c=lista_general[sdlk.Focus]
						if  (eventos.button.button==1) //and (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
							c.soltado (c)
							
						//c.soltar_botones(c.Index)
									
				when EventType.MOUSEBUTTONDOWN
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					sdlk.Focus=-1
					var contr= ultimo_de_lista(lista_profundidad)
					case  (eventos.button.button)
						when 1
							while contr>=0
								var c=lista_general[lista_profundidad[contr]]
								if (c.Visible) and (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
									var x =(uint8)(sdlk.Puntero_x- c.dr.x) //indica el lugar xy desde donde se engancha el boton
									var y = (uint8)(sdlk.Puntero_y - c.dr.y) // para poder ser arrastrado manteniendo este punto.
									if (c.Nombre=="Imagen") 
										if (c.get_alpha(x, y)>5) 
											c.izq_pulsado(c)
											break
									else
										c.izq_pulsado(c)
										break
								contr--

				when EventType.MOUSEMOTION
					Puntero_x=eventos.button.x
					Puntero_y=eventos.button.y
					
					var contr= ultimo_de_lista(lista_profundidad)
					case  (eventos.button.button)
						when 1
							while contr>=0
								var c=lista_general[lista_profundidad[contr]]
								if (sdlk.colision_xy(eventos.button.x,eventos.button.y,c))
									c.izq_sobre(c)
									break
								contr--
				
							
						
				when EventType.KEYUP
					Teclas[SDL.Keyboard.get_keyname(eventos.key.keysym.sym)]=false
					sdlk.tecleado_up(SDL.Keyboard.get_keyname(eventos.key.keysym.sym))
				
				when EventType.TEXTINPUT
					if sdlk.Focus>-1 do 
						var c=lista_general[sdlk.Focus]
						if c.Nombre=="Entrada"
							if valida_sp(evento.text.text)
								c.tecleado(c.Index,evento.text.text)
					sdlk.tecleado(evento.text.text)
		
				when EventType.KEYDOWN
					if sdlk.Focus>-1 do 
						var c=lista_general[sdlk.Focus]
						if c.Nombre=="Entrada"
							//print "Teclado:"+SDL.Keyboard.get_keyname(eventos.key.keysym.sym)
							c.tecleado(c.Index,"Teclado:"+SDL.Keyboard.get_keyname(eventos.key.keysym.sym))
							if SDL.Keyboard.get_keyname(eventos.key.keysym.sym)=="Return"
								c.enter(c)
					
					Teclas[SDL.Keyboard.get_keyname(eventos.key.keysym.sym)]=true
					sdlk.tecleado(SDL.Keyboard.get_keyname(eventos.key.keysym.sym))





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



class Control:Object
	Ref: int
	Index: int
	Nombre: string
	Imagen_0 : SDL.Surface
	Imagen_1 : SDL.Surface
	Imgfondo : SDL.Surface
	Imagen_sobre : SDL.Surface
	
	Textura_0 : SDL.Texture
	Textura_1 : SDL.Texture
	Texfondo : SDL.Texture
	Textura_sobre : SDL.Texture
	
	Texto: string
	Valor_str: string
	Valor_str2: string
	Valor_int: int
	Valor_int2: int
	Valor_bool:bool
	
	Resaltar_Sobre:bool=true
	Visible:bool=true
	Fondo_Visible:bool=true
	Arrastrable:bool=true
	Tomado:bool=false
	Tomado_x:int=0
	Tomado_y:int=0
	TamanoTexto:int=12
	TextoVisible:bool=true
	ColorTexto:SDL.Color
	ColorFondo:SDL.Color
	ColorControl:SDL.Color
	ColorBorde:SDL.Color
	Borde:bool=true
	Espacio_x:int=(int)(20*rw) // es el espacio que contienen las etiquetas entre el fin de control y la letra
	Espacio_y:int=0
	Valor_num: int
	Tamano_x: int
	Tamano_y: int
	sr:SDL.Rect
	dr:SDL.Rect
	srt:SDL.Rect
	drt:SDL.Rect
	Pulsado:int=0
	Mascara:list of uchar
	FiguraFondo:bool=false
	
	event izq_pulsado(c:Control)
	event der_pulsado(c:Control)
	event soltado(c:Control)
	event sobre(c:Control)
	event izq_sobre(c:Control)
	event tecleado(a:int,t:string)
	event enter(c:Control)
	event update(c:Control)
	
	init 
		this.Valor_str=""
		this.Texto=""
		
	def virtual pinta()
		pass
	
	def virtual set_Posicion_x(a:int)
		pass
	
	def virtual get_Ancho():int
		return 0
		
	def virtual get_Alto():int
		return 0
		
	def virtual set_ColorBorde(r:uchar,g:uchar,b:uchar,a:uchar)
		pass
		
	def virtual set_ColorControl(r:uchar,g:uchar,b:uchar,a:uchar)
		pass
	
	def virtual set_ColorTexto(r:uchar,g:uchar,b:uchar,a:uchar)
		pass
	
	def virtual set_Posicion_y(a:int)
		pass
	
	def virtual get_Posicion_x():int
		return 0

	def virtual set_Texto(a:string)
		pass
	
	def virtual get_Texto():string
		return ""
	
	def virtual get_Posicion_y():int
		return 0
	
	def virtual set_Posicion(a:int,b:int)
		pass
	def virtual get_alpha (x:int,y:int): uchar
		return 0
	def soltar_botones (a:int)
		var c=sdlk.lista_general[a]
		if c.Pulsado==1 do c.Pulsado=2
	def muere()
		if sdlk.lista_profundidad.contains(this.Index)
			var x=sdlk.lista_profundidad.index_of(this.Index)
			sdlk.lista_profundidad.remove_at(x)
			for var i=0 to ultimo_de_lista(sdlk.lista_general)
				if sdlk.lista_general[i].Index == this.Index
					var vacio=new Control
					sdlk.lista_general.remove_at(i)
					sdlk.lista_general.insert(i,vacio)
					

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	IMAGEN

// La clase que define los botones de imagen					
class Imagen: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	pasando:bool=false
	
	construct (referencia:int,posicion_x:int,posicion_y:int, nombreimagen:string)
		
		this.Ref=referencia
		this.Nombre="Imagen"
		this.Texto=" "
		this.Imagen_0= SDLImage.load (nombreimagen)
		this.sr.x = 0;this.sr.y = 0; this.sr.w = (uint16)this.Imagen_0.w; this.sr.h= (uint16)this.Imagen_0.h
		this.Textura_0= new Texture.from_surface(screen, this.Imagen_0)
		
		this.Imagen_sobre= SDLImage.load (nombreimagen)
		sdlk.abrillanta_imagen(ref this.Imagen_sobre)
		this.Textura_sobre= new Texture.from_surface(screen,this.Imagen_sobre)
		
		this.sr.x = 0;this.sr.y = 0; this.sr.w = (uint16)this.Imagen_0.w; this.sr.h= (uint16)this.Imagen_0.h
		this.dr.x = (int16)(posicion_x*rw);  this.dr.y =(int16)(posicion_y*rh);  this.dr.w =(int16)(this.sr.w*rw);   this.dr.h = (int16)(this.sr.h*rh);
		this.Mascara=sdlk.convertir_surface_en_mascara(this.Imagen_0)
		
		
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		this.sobre.connect(control_sobre)
		
	def set_Imagen(nombreimagen:string)
		this.Imagen_0 = load (nombreimagen)
		this.Mascara=sdlk.convertir_surface_en_mascara(this.Imagen_0)
		this.Textura_0= new Texture.from_surface(screen, this.Imagen_0)
		
		this.Imagen_sobre= SDLImage.load (nombreimagen)
		sdlk.abrillanta_imagen(ref this.Imagen_sobre)
		this.Textura_sobre= new Texture.from_surface(screen,this.Imagen_sobre)
		
		
		this.sr.w = (uint16)this.Imagen_0.w;
		this.sr.h = (uint16)this.Imagen_0.h;
		this.dr.w = (uint16)(this.Imagen_0.w*rw);
		this.dr.h = (uint16)(this.Imagen_0.h*rh);
		

	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)(x*rw)
		this.dr.y=(int16)(y*rh)
	
		
	def override set_Posicion_y(y:int)
		this.dr.y=(int16)(y*rh)

	def override set_Posicion_x(x:int)
		this.dr.x=(int16)(x*rw)
	
	def override get_Posicion_x(): int 
		return (int)(this.dr.x/rw)
	
	def override get_Posicion_y(): int 
		return (int)(this.dr.y/rh)


	def override get_Ancho(): int 
		return (int)(this.dr.w/rw)

	def override get_Alto(): int 
		return (int)(this.dr.h/rh)
		
		
	def set_Tamano(w:int, h:int)
		this.dr.w  = (uint16)(w*rw);
		this.dr.h  = (uint16)(h*rh);
		
		wa:double=(double)w/(double)this.Imagen_0.w
		ha:double=(double)h/(double)this.Imagen_0.h
		
		this.Imagen_0=RotoZoom.zoom(this.Imagen_0,(wa*rw),(ha*rh),1);
		this.Mascara=sdlk.convertir_surface_en_mascara(this.Imagen_0)
		
		
	def control_sobre(c:Control)
		
		this.Tomado_x = sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
		this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		if (c.Resaltar_Sobre) and (get_alpha((int)this.Tomado_x, (int)this.Tomado_y)>5) and (c.Visible)
			pasando=true
		
	// evento soltado
	def control_soltado(c:Control)
		if c.Arrastrable
			c.Tomado=false
		sdlk.lista_profundidad.remove(c.Index)
		sdlk.lista_profundidad.add(c.Index)
		
	// evento pulsado
	def control_pulsado(c:Control)
		this.Tomado_x = sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
		this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		// "%d",get_alpha((int8)this.Tomado_x, (int8)this.Tomado_y)
		if (c.Arrastrable) and (get_alpha((int)this.Tomado_x, (int)this.Tomado_y)>5) and (c.Visible)
			c.Tomado=true
			if not c.FiguraFondo
				sdlk.Focus=c.Index
			else
				sdlk.Focus=-1
		
			
		
	def override get_alpha (x:int,y:int): uchar
		dire:uint8*
		alpha:uchar=0
		
		if (x<this.Imagen_0.w) and (y<this.Imagen_0.h)
			this.Imagen_0.do_lock()
			dire=this.Imagen_0.pixels 
			dire+= (y*this.Imagen_0.pitch)+ (x*this.Imagen_0.format.BytesPerPixel)
			case this.Imagen_0.format.BytesPerPixel
				when 1,2,3
					alpha=255
				when 4
					dire+=+3
					alpha=*dire
			this.Imagen_0.unlock()
		return alpha

	def override pinta ()
		if this.Visible
			if (this.Tomado) 
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x > sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y >sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5

			if this.pasando	
				screen.copy (this.Textura_sobre, this.sr , this.dr);
			else
				screen.copy (this.Textura_0, this.sr , this.dr);
			pasando=false
				
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	ETIQUETA

class Etiqueta: Control
	color:SDL.Color
	Fuente:SDLTTF.Font
	
	linea_texto: string
	linea_imagen:  SDL.Surface
	linea_textura:  SDL.Texture
	linea_srt:  SDL.Rect
	linea_drt: SDL.Rect

	init 
		linea_texto=""
		
	construct (referencia:int,posicion_x:int,posicion_y:int,texto:string)
		this.Ref=referencia
		this.dr.x = (int16)(posicion_x*rw) 
		this.dr.y =(int16)(posicion_y*rh);
		
		this.Nombre="Etiqueta"
		this.Texto=texto
		
		this.ColorTexto.b=0
		this.ColorTexto.g=0
		this.ColorTexto.r=0
		this.ColorTexto.a=255
		this.TamanoTexto=155
		
		this.ColorControl.r=23
		this.ColorControl.g=223
		this.ColorControl.b=222
		this.ColorControl.a=255
		
		this.ColorBorde.r=0
		this.ColorBorde.g=0
		this.ColorBorde.b=0
		this.ColorBorde.a=255
		
		this.Imagen_0= SDLImage.load (directorio_datos+"irudiak/iman.png")
		this.Textura_0= new Texture.from_surface(screen,this.Imagen_0)
		
		
		set_Texto(texto)
		
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
	
		
	def override set_Texto(nuevotexto:string)
		if nuevotexto!=""
			fuente:unowned Font= sdlk.cargafuentekatamotz2((int)(this.TamanoTexto*rw))
			linea_imagen= fuente.render_blended_utf8 (nuevotexto,this.ColorTexto)
			linea_textura = new Texture.from_surface(screen, linea_imagen)
			
			// source del texto
			this.srt.x = 0;
			this.srt.y = 0;                 
			this.srt.w = (int16)(linea_imagen.w)    
			this.srt.h= (int16)(linea_imagen.h)
			
			// direccion del texto
			this.drt.x = (int16)this.dr.x+this.Espacio_x;    // añadimos 20 para la distancia con el borde izquierdo   
			this.drt.y =(int16)this.dr.y;  
			this.drt.w =(int16)(this.srt.w);                  
			this.drt.h = (int16)(this.srt.h);
			
			// source del control (iman.png)
			this.sr.x = 0;                   
			this.sr.y = 0;                 
			this.sr.w = (int16)(this.Imagen_0.w);    
			this.sr.h= (int16)(this.Imagen_0.h);
			
			// ancho del control; la direccion no la movemos
			this.dr.w = (int16)(this.srt.w + this.Espacio_x*2);      // se añaden 40 para dar distancia con el borde derecho 20 izquierdo+20 derecho.
			this.dr.h = (int16)(this.srt.h);
			
		this.Texto = nuevotexto;
		
	def override get_Texto():string
		return this.Texto
		
	def override set_ColorBorde(r:uchar,g:uchar,b:uchar,a:uchar)
		this.ColorBorde.r=r
		this.ColorBorde.g=g
		this.ColorBorde.b=b
		this.ColorBorde.a=a
		
	def override set_ColorTexto(r:uchar,g:uchar,b:uchar,a:uchar)
		this.ColorTexto.r=r
		this.ColorTexto.g=g
		this.ColorTexto.b=b
		this.ColorTexto.a=a
		set_Texto(this.Texto)
	
	def set_ColorTexto_rgb(r:uchar,g:uchar,b:uchar)
		this.ColorTexto.r=r
		this.ColorTexto.g=g
		this.ColorTexto.b=b
		this.ColorTexto.a=255
		set_Texto(this.Texto)
	
	def set_TamanoTexto(tamano:int)
		this.TamanoTexto=tamano
		set_Texto(this.Texto)
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)x
		this.dr.y=(int16)y
		this.drt.x=(int16)(x*rw+this.Espacio_x)
		this.drt.y=(int16)(y*rh+this.Espacio_x)

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)(y*rh)
		this.drt.y=(int16)(y*rh+this.Espacio_x)
		set_Texto(this.Texto)
		
	def override set_Posicion_x(x:int)
		this.dr.x=(int16)(x*rw)
		this.drt.x=(int16)(x*rw+this.Espacio_x)
		set_Texto(this.Texto)
		
	def override get_Posicion_x(): int 
		return (int)(this.dr.x/rw)
	
	def override get_Posicion_y(): int 
		return (int)(this.dr.y/rh)

	def override get_Ancho(): int 
		return (int)(this.dr.w/rw)

	def override get_Alto(): int 
		return (int)(this.dr.h/rh)	
			
	def override set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		sdlk.colorea_imagen(ref this.Imagen_0,R,G,B)
		this.Textura_0= new Texture.from_surface(screen,this.Imagen_0)
		
		
	def control_soltado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=false
		
		sdlk.lista_profundidad.remove(c.Index)
		sdlk.lista_profundidad.add(c.Index)
		
		
	// evento pulsado
	def control_pulsado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=true
			this.Tomado_x =sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
			this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		sdlk.Focus=c.Index
			
	def override pinta ()
		if this.Visible
			if (this.Tomado) 
				// verifica que no se va fuera de la pantalla desde las x
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x  >sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				
				// verifica que no se va fuera de la pantalla desde las y
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y>sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5
				
				
				this.drt.x = (int16)(this.dr.x+this.Espacio_x);         
				this.drt.y = (int16)(this.dr.y );         
				
			if this.Fondo_Visible
				if this.Borde
					//dibuja rectangulo con borde
					//sdlk.Rectangulo (screen,this.dr,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.a)
					//screen.copy (this.Textura_0, this.srt , this.dr);
					screen.copy (this.Textura_0, this.sr , this.dr);
				
				else
					//Dibuja rectangulo sin borde
					sdlk.Rectangulo (screen,this.dr,this.ColorControl.r,this.ColorControl.g,this.ColorControl.b,this.ColorControl.a)	
					
			if this.TextoVisible
				
				screen.copy (linea_textura, this.srt , this.drt);



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 	CUADROREDONDO

class CuadradoRedondo: Control
	color:SDL.Color
	
	init
		pass
		
	construct (referencia:int,posicion_x:int,posicion_y:int,w:int,h:int)
		this.Ref=referencia
		this.Nombre="CuadradoRedondo"
		
		this.ColorTexto.b=0
		this.ColorTexto.g=0
		this.ColorTexto.r=0
		this.TamanoTexto=15
		
		this.ColorControl.r=23
		this.ColorControl.g=223
		this.ColorControl.b=222
		this.ColorControl.a=150
		
		this.Imagen_0= SDLImage.load (directorio_datos+"irudiak/iman.png")
		this.Textura_0= new Texture.from_surface(screen,this.Imagen_0)
		
		this.sr.x = 0;                   
		this.sr.y = 0;                 
		this.sr.w = (uint16)this.Imagen_0.w;    
		this.sr.h= (uint16)this.Imagen_0.h;
		
		this.dr.x = (int16)(posicion_x*rw);  
		this.dr.y =(int16)(posicion_y*rh);  
		this.dr.w = (int16)(w*rw);                   
		this.dr.h = (int16)(h*rh);
		sdlk.genera_elemento(this)
		
		this.izq_pulsado.connect(control_pulsado)
		this.soltado.connect(control_soltado)
		
		
	
	def override set_Posicion(x:int,y:int)
		this.dr.x=(int16)(x*rw)
		this.dr.y=(int16)(y*rh)

	def override set_Posicion_y(y:int)
		this.dr.y=(int16)(y*rh)
		
	def override set_Posicion_x(x:int)
		this.dr.x=(int16)(x*rw)
		
	def override get_Posicion_x(): int 
		return (int)(this.dr.x/rw)
	
	def override get_Posicion_y(): int 
		return (int)(this.dr.y/rh)


	def override get_Ancho(): int 
		return (int)(this.dr.w/rw)

	def override get_Alto(): int 
		return (int)(this.dr.h/rh)
		
			
	def override set_ColorControl(R:uchar,G:uchar,B:uchar,A:uchar)
		this.ColorControl.r=R
		this.ColorControl.g=G
		this.ColorControl.b=B
		
		sdlk.colorea_imagen(ref this.Imagen_0,R,G,B)
		this.Textura_0= new Texture.from_surface(screen,this.Imagen_0)
		
	def control_soltado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=false
	
	// evento pulsado
	def control_pulsado(c:Control)
		var a=c.Index
		if sdlk.lista_general[a].Arrastrable
			sdlk.lista_general[a].Tomado=true
			this.Tomado_x =sdlk.Puntero_x- this.dr.x //indica el lugar xy desde donde se engancha el boton
			this.Tomado_y = sdlk.Puntero_y - this.dr.y // para poder ser arrastrado manteniendo este punto.
		if not c.FiguraFondo
			sdlk.Focus=c.Index
			sdlk.lista_profundidad.remove(c.Index)
			sdlk.lista_profundidad.add(c.Index)

		else
			sdlk.Focus=-1
			
			
	def override pinta ()
		if this.Visible
			if this.Tomado
				this.dr.x=(int16)sdlk.Puntero_x-(int16)this.Tomado_x
				if (this.dr.x+this.dr.w <0) do this.dr.x=5
				if (this.dr.x >sdlk.Ancho) do this.dr.x=sdlk.Ancho-(int16)this.dr.w-5
				this.dr.y=(int16)sdlk.Puntero_y-(int16)this.Tomado_y
				if (this.dr.y+this.dr.h <0) do this.dr.y=5
				if (this.dr.y  >sdlk.Alto) do this.dr.y=sdlk.Alto-(int16)this.dr.h-5
			
			screen.copy (this.Textura_0, this.sr , this.dr);
				
