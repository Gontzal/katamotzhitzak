uses Gee
uses SDL
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

class Sonidos: Object
	
	clik  :SDLMixer.Chunk
	botella:SDLMixer.Chunk 
	gaizki: SDLMixer.Chunk 
	ondo0: SDLMixer.Chunk 
	ondo1: SDLMixer.Chunk 
	ondo2: SDLMixer.Chunk 
	ondo3: SDLMixer.Chunk 
	ondo4: SDLMixer.Chunk 
	ondo5: SDLMixer.Chunk 
	sartu: SDLMixer.Chunk 
	canal: SDLMixer.Channel
	musica: SDLMixer.Music
	
	init
		var directorio_sonidos="soinuak/zaratak/"
		botella= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"botella.ogg")
		gaizki= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"boing.ogg")
		ondo0= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo0.ogg")
		ondo1= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo1.ogg")
		ondo2= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo2.ogg")
		ondo3= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo3.ogg")
		ondo4= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo4.ogg")
		ondo5= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"ondo5.ogg")
		clik= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"click.ogg")
		sartu= new SDLMixer.Chunk.WAV (directorio_datos+directorio_sonidos+"entrando1.ogg")
	
	def play (s:string,archivo:string="")
		case s
			when "blub"
				canal.play(botella,0)
			when "clik"
				canal.play(clik,0)
			when "gaizki"
				canal.play(gaizki,0)
				
			when "sartu"
				canal.play(sartu,0)
				
			when "ondo"
				var n=Random.int_range(0,6)
				case n
					when 0
						canal.play(ondo0,0)
					when 1
						canal.play(ondo1,0)
					when 2
						canal.play(ondo2,0)
					when 3
						canal.play(ondo3,0)
					when 4
						canal.play(ondo4,0)
					when 5
						canal.play(ondo5,0)
			when "archivo"
				musica= new SDLMixer.Music (archivo)
				musica.play(1)
