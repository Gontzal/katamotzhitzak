
uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class Juego3:Object
	
	salir : Imagen
	fin:bool
	letras:list of Etiqueta
	letras_opcion : list of Etiqueta
	imagen:Imagen	
	solucion: int
	letra_tomada:string
	letra_escondida:string
	miarchivo:string
	misletras: list of string
	misletras_opcion: list of string
	consonantes:array of string={"b","d","f","g","h","j","k","l","m","n","p","r","s","t","x","z"}
	vocales:array of string={"a","e","i","o","u"}
	init
		letras= new list of Etiqueta
		letras_opcion= new list of Etiqueta
		misletras= new list of string
		misletras_opcion=new list of string
		
	def inicio()
		print "juego 3"
		sdlk.muere_todo()
		
		// eligiendo una palabra y dividiendola en letras
		miarchivo=datos.toma_palabra_alumno()
		var mipalabra= miarchivo.replace("_","")
		solucion= Random.int_range (0,ultima(mipalabra) )
		letra_escondida=toma_cadena (mipalabra,solucion)
		
		misletras_opcion.clear()
		
		//añade letras de opcion
		if (letra_escondida in consonantes)
			misletras_opcion.add(letra_escondida)
			misletras_opcion.add(selecciona_item_str_azar_array (consonantes))
			misletras_opcion.add(selecciona_item_str_azar_array (consonantes))
			misletras_opcion.add(selecciona_item_str_azar_array (consonantes))
		else
			misletras_opcion.add(letra_escondida)
			misletras_opcion.add(selecciona_item_str_azar_array (vocales))
			misletras_opcion.add(selecciona_item_str_azar_array (vocales))
			misletras_opcion.add(selecciona_item_str_azar_array (vocales))
		desordena_lista_string (ref misletras_opcion)
		
		var misletras=datos.crea_lista_de_letras_desde_palabra (mipalabra)
		misletras[solucion]="?"
		print mipalabra
		letras.clear()
		letras_opcion.clear()
		
		
		imagen= new Imagen (0,550,200, directorio_datos+"irudiak/hitzak/"+datos.transforma_archivo_str(miarchivo)+".png") 
		imagen.set_Tamano(250,250)
		imagen.Arrastrable=false
		imagen.izq_pulsado.connect(on_imagen)
		
		salir= new Imagen(0,5,600,directorio_datos+"irudiak/atzera.png")
		salir.set_Tamano(150,150)
		salir.Arrastrable=false
		salir.izq_pulsado.connect(on_salir)
		
		// letras de la palabra
		var posx=0
		for var i=0 to ultimo_de_lista(misletras)
			letras.add( new Etiqueta (i,posx,500,misletras[i]) )
			letras.last().set_TamanoTexto(120)
			letras.last().Valor_str=misletras[i]
			letras.last().Valor_bool=false
			letras.last().Arrastrable=false
			if letras.last().Valor_str=="?" do letras.last().Fondo_Visible=false
			posx+=letras.last().get_Ancho()
		
		// centra las letras en la pantalla
		var nuevax=(int)(sdlk.Ancho/rw)/2-posx/2
		for var i=0 to ultimo_de_lista(misletras)
			letras[i].set_Posicion_x(nuevax)
			nuevax+=letras[i].get_Ancho()
		
		
		// letras para meter en la palabra
		posx=0
		var color=(uchar)Random.int_range(100,250)
		for var i=0 to ultimo_de_lista(misletras_opcion)
			letras_opcion.add( new Etiqueta (i,posx,5,misletras_opcion[i]) )
			letras_opcion.last().set_TamanoTexto(120)
			letras_opcion.last().Valor_str=misletras_opcion[i]
			letras_opcion.last().Valor_bool=false
			letras_opcion.last().Arrastrable=true
			letras_opcion.last().set_ColorControl(100,155,color,-1)
			letras_opcion.last().izq_pulsado.connect (on_tomar_letra)
			letras_opcion.last().soltado.connect (on_soltar_letra)
			
			posx+=letras_opcion.last().get_Ancho()
		
		// centra las letras en la pantalla
		nuevax=(int)(sdlk.Ancho/rw)/2-posx/2
		for var i=0 to ultimo_de_lista(misletras_opcion)
			letras_opcion[i].set_Posicion_x(nuevax)
			nuevax+=letras_opcion[i].get_Ancho()
		
		
		
		// comienza el loop;
		fin=false
		num_ejercicio=-2
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while SDL.Event.poll(out evento)==1		
				if evento.type == SDL.EventType.QUIT
					fin= true
					break
				else 
					sdlk.toma_eventos(evento)
			// Realiza los cambios de juego necesarios
			//sdlk.update_control()
			//pintar el fondo
			pinta_fondo_color()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(tiempo)

	def on_imagen(c:Control)
		sonidos.play("archivo",directorio_datos+"soinuak/hitz_soinuak_eu/"+datos.transforma_archivo_str(miarchivo)+".ogg")
		pass
		
	def on_tomar_letra(c:Control)
		letra_tomada=c.Valor_str
		pass
		
	def on_soltar_letra(c:Control)
		if sdlk.colision_mascara(letras[solucion],c) 
			if letras[solucion].Valor_str=="?"
				c.Valor_bool=true
				letras[solucion].Valor_str=letra_tomada
				c.set_Posicion_y((letras[solucion].get_Posicion_y()+letras[solucion].get_Alto()/2)-(c.get_Alto()/2))
				c.set_Posicion_x((letras[solucion].get_Posicion_x()+letras[solucion].get_Ancho()/2)-(c.get_Ancho()/2))
				corrige()
		else
			if letra_tomada==letras[solucion].Valor_str
				letras[solucion].Valor_str="?"
		
	def on_salir()
		fin=true
		num_ejercicio=-2
		
		

	def corrige()
		if letra_tomada==letra_escondida
			sonidos.play("ondo")
			fin=true
			num_ejercicio=3
		else
			sonidos.play("gaizki")
			
	def pinta_fondo_color()
		//sdlk.Rectangulo_xywh( screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
		screen.copy (wall2, null , {0,0,sdlk.Ancho,sdlk.Alto});

