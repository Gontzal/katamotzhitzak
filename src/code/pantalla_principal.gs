
uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class Pantalla_principal:Object
	
	imagen:list of Imagen
	fin:bool
	maila:list of Imagen
	salir:Imagen
	fondo_maila: CuadradoRedondo
	init
		imagen=new list of Imagen
		maila=new list of Imagen
	def inicio()
		print "pantalla principal"
		sdlk.muere_todo()
		imagen.clear()
		maila.clear()
		
		var x=0  // el numero de ejercicio que serapasado como c.Ref (arg 1 de Imagen) para identificar el ejercicio pulsado en la funcion imagen.
		for var y=0 to 1
			for var i=0 to 2
				imagen.add(new Imagen (x,100+350*i,100+350*y, directorio_datos+"irudiak/img"+tostring(4,x)+".png") )
				imagen.last().set_Tamano(280,280)
				imagen.last().Arrastrable=false
				imagen.last().izq_pulsado.connect(on_imagen)
				x+=1
		
		fondo_maila= new CuadradoRedondo (0,1190,10,160,420)
		fondo_maila.Arrastrable=false
		fondo_maila.FiguraFondo=true
		fondo_maila.set_ColorControl(10,200,200,255)
		
		var posy=10
		for var i=0 to 2
			maila.add(new Imagen(i,1200,posy,directorio_datos+"irudiak/pinguino.png"))
			maila.last().set_Tamano(100+i*25,100+i*25)
			maila.last().Arrastrable=false
			maila.last().izq_pulsado.connect(on_maila)
			maila.last().set_Posicion_x(1270-maila.last().get_Ancho()/2 )
			posy+= maila.last().get_Alto()+20
		
		maila[nivel].set_Imagen(directorio_datos+"irudiak/pinguino2.png")
		maila[nivel].set_Tamano(100+25*nivel,100+25*nivel)
		
		var play= new Imagen (0,1190,450, directorio_datos+"irudiak/play.png")
		play.set_Tamano(130,130)
		play.Arrastrable=false
		play.izq_pulsado.connect(on_play)
		
		salir= new Imagen(0,1190,600,directorio_datos+"irudiak/atzera.png")
		salir.set_Tamano(150,150)
		salir.Arrastrable=false
		salir.izq_pulsado.connect(on_salir)
		
		// comienza el loop;
		num_ejercicio=-2
		fin=false
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while SDL.Event.poll(out evento)==1		
				if evento.type == SDL.EventType.QUIT
					fin= true
					num_ejercicio=-1
					break
				else
					sdlk.toma_eventos(evento)
			// Realiza los cambios de juego necesarios
			//sdlk.update_control()
			//pintar el fondo
			pinta_fondo_color()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(tiempo)
		
	//Esta funcion reconoce el pulsado de la imagen de ejercicio y envia fin del bucle y vuelta al bucle prncipal de katandroid con
	// el numero de ejercicio pasado a traves de num_ejercicio recogido de c.ref.
	def on_imagen(c:Control)
		sonidos.play("sartu")
		num_ejercicio=c.Ref
		fin=true
		
	def pinta_fondo_color()
		//sdlk.Rectangulo_xywh( screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
		screen.copy (wall, null , {0,0,sdlk.Ancho,sdlk.Alto});
	
	def on_maila(c:Control)
		maila[nivel].set_Imagen(directorio_datos+"irudiak/pinguino.png")
		maila[nivel].set_Tamano(100+25*nivel,100+25*nivel)
		
		case c.Ref
			when 0
				datos.alumno_palabras=datos.crea_lista_de_palabras_segun_medida(2, 5, datos.todas_palabras)
				nivel=0
				sonidos.play("archivo",directorio_datos+"soinuak/programakoak_eu/zailtasun0.ogg")
			when 1
				datos.alumno_palabras=datos.crea_lista_de_palabras_segun_medida(5, 8, datos.todas_palabras)
				nivel=1
				sonidos.play("archivo",directorio_datos+"soinuak/programakoak_eu/zailtasun1.ogg")
			when 2
				datos.alumno_palabras=datos.crea_lista_de_palabras_segun_medida(9, 15, datos.todas_palabras)
				nivel=2
				sonidos.play("archivo",directorio_datos+"soinuak/programakoak_eu/zailtasun2.ogg")
		maila[nivel].set_Imagen(directorio_datos+"irudiak/pinguino2.png")
		maila[nivel].set_Tamano(100+25*nivel,100+25*nivel)
		
		pass
	def on_play()
		fin=true
		play=true // cuando play=true el numero de ejercicio se convierte en aleatorio
		num_ejercicio=-3
		sonidos.play("archivo",directorio_datos+"soinuak/programakoak_eu/nahasteborraste.ogg")
	def on_salir()
		fin=true
		num_ejercicio=-1
		play=false
		
