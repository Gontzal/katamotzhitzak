
uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class Juego1:Object
	
	salir : Imagen
	etiqueta : Etiqueta
	fin:bool
	iman:CuadradoRedondo
	imagen: list of Imagen	
	palabra:string
	miarchivo:string
	solucion : int
	archivos:list of string
	
	init
		imagen= new list of Imagen
		archivos=new list of string
		
	def inicio()
		print "juego 1"
		sdlk.muere_todo()
		
		
		// eligiendo una palabra y dividiendola en letras
		archivos.clear()
		archivos= datos.toma_palabras_alumno(3)
		miarchivo= archivos[0].replace("_","")
		desordena_lista_string (ref archivos)
		solucion=-1
		
		etiqueta= new Etiqueta ( 0, 1,1,miarchivo )
		var posx=sdlk.get_Ancho_Pantalla()/2-etiqueta.get_Ancho()/2
		etiqueta.set_Posicion_x(posx)
		etiqueta.Arrastrable=false
		
		imagen.clear()
		for var i=0 to 3
			imagen.add( new Imagen (i,170+270*i,450, directorio_datos+ "irudiak/hitzak/"+datos.transforma_archivo_str(archivos[i])+".png") )
			imagen.last().set_Tamano(200,200)
			imagen.last().izq_pulsado.connect(on_tomar_imagen)
			imagen.last().soltado.connect(on_soltar_imagen)
		
		salir= new Imagen(0,5,600,directorio_datos+"irudiak/atzera.png")
		salir.set_Tamano(150,150)
		salir.Arrastrable=false
		salir.izq_pulsado.connect(on_salir)
		
		iman= new CuadradoRedondo (0,560,200,220,220)
		iman.Arrastrable=false
		iman.FiguraFondo=true			
		iman.set_ColorControl(170,170,255,-1)


		
		// comienza el loop;
		fin=false
		num_ejercicio=-2
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while SDL.Event.poll(out evento)==1		
				if evento.type == SDL.EventType.QUIT
					fin= true
					break
				else 
					sdlk.toma_eventos(evento)
			// Realiza los cambios de juego necesarios
			//sdlk.update_control()
			//pintar el fondo
			pinta_fondo_color()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(tiempo)

	def on_imagen(c:Control)
		sonidos.play("archivo",directorio_datos+"soinuak/hitz_soinuak_eu/"+datos.transforma_archivo_str(miarchivo)+".ogg")
		pass
		
		
	def on_salir()
		fin=true
		num_ejercicio=-2
		
		
	def pinta_fondo_color()
		//sdlk.Rectangulo_xywh( screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
		screen.copy (wall2, null , {0,0,sdlk.Ancho,sdlk.Alto});

	def on_tomar_imagen(c:Control)
		c.Valor_bool=false
		if c.Ref == solucion
			solucion=-1
		
		
	def on_soltar_imagen (c:Control)
		if sdlk.colision_mascara(iman,c) 
			if solucion==-1 
				c.Valor_bool=true
				solucion=c.Ref
				c.set_Posicion_y((iman.get_Posicion_y()+iman.get_Alto()/2)-(c.get_Alto()/2))
				c.set_Posicion_x((iman.get_Posicion_x()+iman.get_Ancho()/2)-(c.get_Ancho()/2))
				corrige()
		
	def corrige()
		print solucion.to_string()
		if miarchivo==archivos[solucion].replace("_","")
			sonidos.play("ondo")
			fin=true
			num_ejercicio=1
		else
			sonidos.play("gaizki")
