
uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class Juego4:Object
	
	salir : Imagen
	fin:bool
	letras:list of Etiqueta
	letra: Etiqueta
	imagen:Imagen	
	palabra:string
	miarchivo:string
	letrastr:string
	delay:int
	posx:int
	misletras:list of string
	init
		letras= new list of Etiqueta
		misletras= new list of string

	def inicio()
		print "juego 4: Ikasleak hizkia non dagoen aurkitu behar du."
		sdlk.muere_todo()
	
		// eligiendo una palabra y dividiendola en letras
		misletras.clear()
		miarchivo=datos.toma_palabra_alumno()
		var mipalabra= miarchivo.replace("_","")
		
		misletras=datos.crea_lista_de_letras_desde_palabra (mipalabra)
		letras.clear()	
		letrastr= selecciona_item_str_azar (misletras)
		
		// Ikasleak aurkitu behar duen hizkia
		var color= (uchar)Random.int_range(100,250)
		letra= new Etiqueta (0,600,10,letrastr)
		letra.set_TamanoTexto(100)
		letra.Arrastrable=true
		letra.soltado.connect(on_soltar_letra)
		letra.set_ColorControl(10,color,70,-1)
		
		// 
		imagen= new Imagen (0,550,200, directorio_datos+"irudiak/hitzak/"+datos.transforma_archivo_str(miarchivo)+".png") 
		imagen.set_Tamano(250,250)
		imagen.Arrastrable=false
		imagen.izq_pulsado.connect(on_imagen)

		salir= new Imagen(0,5,600,directorio_datos+"irudiak/atzera.png")
		salir.set_Tamano(150,150)
		salir.Arrastrable=false
		salir.izq_pulsado.connect(on_salir)
		
		
		posx=0
		for var i=0 to ultimo_de_lista(misletras)
			letras.add( new Etiqueta (i,posx,510,"a") )
			letras.last().set_TamanoTexto(110)
			letras.last().Valor_str= misletras[i]
			letras.last().Valor_bool=false
			letras.last().TextoVisible=false
			letras.last().set_ColorControl(100,155,199,-1)
			posx+=letras.last().get_Ancho()+5
		
		// centra las letras en la pantalla
		var nuevax=(int)(sdlk.Ancho/rw)/2-posx/2
		for var i=0 to ultimo_de_lista(misletras)
			letras[i].set_Posicion_x(nuevax)
			nuevax+=letras[i].get_Ancho()+5
		
		
		
		// comienza el loop;
		fin=false
		num_ejercicio=-2
		delay=5
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while SDL.Event.poll(out evento)==1		
				if evento.type == SDL.EventType.QUIT
					fin= true
					break
				else 
					sdlk.toma_eventos(evento)
			// Realiza los cambios de juego necesarios
			//sdlk.update_control()
			//pintar el fondo
			pinta_fondo_color()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(delay)
			delay=5
			
	def on_imagen(c:Control)
		sonidos.play("archivo",directorio_datos+"soinuak/hitz_soinuak_eu/"+datos.transforma_archivo_str(miarchivo)+".ogg")
	
	def on_soltar_letra (c:Control)
		for var n=0 to ultimo_de_lista(misletras)
			if sdlk.colision_mascara(letras[n],c) 
				if letrastr==letras[n].Valor_str
					sonidos.play("ondo")
					num_ejercicio=4
					fin=true
				else
					sonidos.play("gaizki")
					num_ejercicio=4
					fin=true
				
				
					
				// que se vea por unos instantes para reconocer la palabra
				for var i=0 to ultimo_de_lista(letras)
					letras[i].TextoVisible=true
					letras[i].set_Texto(letras[i].Valor_str)
				// centra las letras en la pantalla
				var nuevax=(int)(sdlk.Ancho/rw)/2-posx/2
				for var i=0 to ultimo_de_lista(misletras)
					letras[i].set_Posicion_x(nuevax)
					nuevax+=letras[i].get_Ancho()+5
				c.Visible=false
				pinta_fondo_color()
				sdlk.pintar()
				SDL.Timer.delay(2000)		
	
				//MessageBox.simple_show (MessageBoxFlags.INFORMATION,"Missing file", "File is missing. Please reinstall the program.", window);

		
	def on_salir()
		fin=true
		num_ejercicio=-2
	
		

			
	def pinta_fondo_color()
		//sdlk.Rectangulo_xywh( screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
		screen.copy (wall2, null , {0,0,sdlk.Ancho,sdlk.Alto});

