/*
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

// valac -o "%e" --pkg gee-1.0 --pkg sdl2 --pkg sdl2-mixer --pkg sdl2-image  --pkg sdl2-gfx --pkg sdl2-ttf -X -lSDL2_ttf  -X -lSDL2_gfx -X -lSDL2_image -X -lSDL2_mixer   ../sdlk/cadenas.gs ../sdlk/sdlk20.gs traduccion.gs j000.gs j001.gs j002.gs j003.gs j004.gs  j005.gs j100.gs j101.gs j102.gs  j103.gs j104.gs j200.gs contrasena.gs sonidos.gs datos.gs menuprincipal.gs "%f" --Xcc=-I/usr/include/SDL2
// La biblioteca sdlk2 se encuentra un nivel antes del directorio de katamotz-zenbakiak puesto que debe de tener un desarrollo diferenciado con el programa.
// Hasiera 2013ko ekainaren 23a

uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib

//general
sdlk:SdlkMinimal
evento: SDL.Event
comando_menu:int
window:SDL.Window;
screen:SDL.Renderer
datos:Datos
sonidos:Sonidos
directorio_datos:string
alto_real:int16=768
ancho_real:int16=1336
alto:int16=0
ancho:int16=0
rw:double=0.0 // la razon que será usada para calcular el ancho relativo para adaptarse a las diferentes pantallas.
rh:double=0.0 // la razon que será usada para calcular el alto relativo para adaptarse a las differentes pantallas
num_ejercicio:int
wall: Texture
wall2: Texture
nivel:int=0
tiempo:int=100
play:bool=false

pantalla_principal:Pantalla_principal 
juego0:Juego0; juego1:Juego1;juego2:Juego2;juego3:Juego3;juego4:Juego4;juego5:Juego5;

/*	
def pinta_fondo(x:uint32,a:void* ):uint32
	Rectangle.fill_rgba(screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
	return (uint32)1
*/

init
	juego0= new Juego0;juego1= new Juego1;juego2= new Juego2;juego3= new Juego3;juego4= new Juego4;juego5= new Juego5;
	pantalla_principal = new Pantalla_principal()
	
	
	//inicializar sdl
	SDL.init (SDL.InitFlag.EVERYTHING|SDLImage.InitFlags.ALL)
	SDLTTF.init ()
	SDLImage.init(SDLImage.InitFlags.ALL)
	SDLMixer.open(44100,SDL.AudioFormat.S16LSB,2,4096);
	window = new Window ("Katamotz 3.30", Window.POS_CENTERED, Window.POS_CENTERED, (int16)ancho,(int16)alto, 
		WindowFlags.FULLSCREEN_DESKTOP);
	screen = new SDL.Renderer (window, -1,  SDL.RendererFlags.ACCELERATED |SDL.RendererFlags.PRESENTVSYNC |SDL.RendererFlags.TARGETTEXTURE);
	
	if args[1] == "linux"
		directorio_datos="../../assets/" /// cuando estamos en linux
	else
		directorio_datos="" /// cuando estamos en android
		
	datos= new Datos()
	sonidos=new Sonidos()

	// determina la razon por la que despues se multiplicaran los valores para adaptarse a cada pantalla
	var h=0; var w=0;
	window.get_size(out w ,out h)
	alto=(int16)h; ancho=(int16)w
	rw=(double)((double)ancho/1366.0)
	rh=(double)((double)alto/768.0)
	//print ancho.to_string()+" "+alto.to_string();+" relacion:"+ r.to_string()
	
	
	// pinta una pantalla mientras se carga.
	var inicio =  SDLImage.load_texture (screen, directorio_datos+ "irudiak/katamotz.png")
	screen.set_draw_color((uint8)Random.int_range(0,254), (uint8)Random.int_range(0,254), (uint8)Random.int_range(0,254), 255)
	screen.fill_rect( { (int)0, (int)0, (int)ancho, (int)alto}) 
	screen.copy(inicio,null, {(int)((ancho/10*2)),(int)((alto/10*2)),(int)((ancho/10*6)),(int)((alto/10*3))})
	screen.present();
	window.update_surface ();
	
	// abriendo archivo de palabras
	datos.todas_palabras=leer_archivo_texto(directorio_datos+"hitzak/imagenes-eu.txt")
	//print "Se anadieron %i palabras",ultimo_de_lista(datos.todas_palabras)
	// establecemos nivel=0 facil. Por lo tanto introducimos en palabras de Alumno las correspondientes al nivel 0
	datos.alumno_palabras=datos.crea_lista_de_palabras_segun_medida(2, 5, datos.todas_palabras)

	// karga sdlkminimal	
	sdlk= new SdlkMinimal(ancho,alto) // Instanciamos el UI sd
	
	
	// carga wall 
	wall =  SDLImage.load_texture (screen, directorio_datos+"irudiak/wall1.png")
	wall2 =  SDLImage.load_texture (screen, directorio_datos+"irudiak/wall2.png")
	
	//LOOP PRINCIPAL
	// bucle principal de la aplicacion, se alterna entre la pantalla_principal y los juegos que son llamados desde continua (ejercicio)
	num_ejercicio=-2
	while true
		continua(num_ejercicio)
	// COMIENZA EL PROGRAMA:
	

def continua(num_ejercicio:int)
	if play and num_ejercicio!=-2 // si esta en modo play elegira un numero aleatorio de ejercicio, siempre y cuando no salga del ejercicio (-2)
		var num=num_ejercicio
		while num==num_ejercicio
			num=Random.int_range(0,6)
		num_ejercicio=num
	case num_ejercicio
		when -1
			finaliza()
		when 0
			juego0.inicio()
		when 1
			juego1.inicio()
		when 2
			juego2.inicio()
		when 3
			juego3.inicio()
		when 4
			juego4.inicio()
		when 5
			juego5.inicio()
		when -2
			play=false 
			pantalla_principal.inicio()
def finaliza()
	
	if (window != null)
		window.destroy ();
	SDL.quit()
	SDLTTF.quit()
	exit(-1)

def extern exit(code:int):void
		
def leer_archivo_texto(archivo:string):list of string
	var lista= new list of string
	var archivotexto = new SDL.RWops.from_file(archivo, "rb");
	if archivotexto==null
		print "no se pudo abrir"
	else
		archivotexto.seek(0,RWFlags.END)
		var tamano=archivotexto.tell()		
		archivotexto.seek(0,RWFlags.SET)
		
		contenido:array of char= new array of char [tamano]
		archivotexto.read(contenido,(size_t)tamano,1)
		print tamano.to_string()
		var cadena=""
		for var i=0 to tamano
			if contenido[i]!=(char)10
				if contenido[i]!=(char)0
					cadena+="%c".printf (contenido[i])
			else
				lista.add(cadena)
				cadena=""
	
	print "fin"
	return lista
