
uses SDL
uses Gee
uses SDLMixer
uses SDLImage
uses SDLGraphics
uses SDLTTF
uses GLib


class Juego0:Object
	
	salir : Imagen
	fin:bool
	iman:CuadradoRedondo
	letras:list of Etiqueta
	imagen:Imagen	
	solucion: list of int
	palabra:string
	miarchivo:string
	palabra_ordenada:string
	palabra_desordenada:string

	
	init
		letras= new list of Etiqueta
		solucion=new list of int
		

	def inicio()
		print "juego 0"
		sdlk.muere_todo()
		
		// eligiendo una palabra y dividiendola en letras
		miarchivo=datos.toma_palabra_alumno()
		var mipalabra= miarchivo.replace("_","")
		var misletras=datos.crea_lista_de_letras_desde_palabra (mipalabra)
		palabra_ordenada=mipalabra
		desordena_lista_string(ref misletras)
		print mipalabra
		solucion.clear()
		letras.clear()
		
		imagen= new Imagen (0,550,200, directorio_datos+"irudiak/hitzak/"+datos.transforma_archivo_str(miarchivo)+".png") 
		imagen.set_Tamano(250,250)
		imagen.Arrastrable=false
		imagen.izq_pulsado.connect(on_imagen)
		
		salir= new Imagen(0,5,600,directorio_datos+"irudiak/atzera.png")
		salir.set_Tamano(150,150)
		salir.Arrastrable=false
		salir.izq_pulsado.connect(on_salir)
		
		var posx=0
		var color=(uchar)Random.int_range(100,250)
		for var i=0 to ultimo_de_lista(misletras)
			letras.add( new Etiqueta (i,posx,10,misletras[i]) )
			letras.last().set_TamanoTexto(120)
			letras.last().soltado.connect(on_soltar_letra)
			letras.last().izq_pulsado.connect(on_tomar_letra)
			letras.last().Valor_str=misletras[i]
			letras.last().set_ColorControl(100,155,color,-1)
			letras.last().Valor_bool=false
			posx+=letras.last().get_Ancho()
			
		
		// centra las letras en la pantalla
		var nuevax=(int)(sdlk.Ancho/rw)/2-posx/2
		for var i=0 to ultimo_de_lista(misletras)
			letras[i].set_Posicion_x(nuevax)
			nuevax+=letras[i].get_Ancho()
		
		
		iman=new CuadradoRedondo(0,300,500,700,letras.last().get_Alto()+50)
		iman.Arrastrable=false
		iman.FiguraFondo=true
		iman.set_ColorControl(170,170,255,-1)
		// comienza el loop;
		fin=false
		num_ejercicio=-2
		while not fin
			// toma eventos.
			sdlk.mira_sobre()
			while SDL.Event.poll(out evento)==1		
				if evento.type == SDL.EventType.QUIT
					fin= true
					break
				else 
					sdlk.toma_eventos(evento)
			// Realiza los cambios de juego necesarios
			//sdlk.update_control()
			//pintar el fondo
			pinta_fondo_color()
			// Pinta los controles
			sdlk.pintar()
			SDL.Timer.delay(tiempo)

	def on_imagen(c:Control)
		sonidos.play("archivo",directorio_datos+"soinuak/hitz_soinuak_eu/"+datos.transforma_archivo_str(miarchivo)+".ogg")
		pass
		
	def on_tomar_letra(c:Control)
		//sonidos.play("clik")
		c.Valor_bool=false
		if solucion.contains(c.Ref)
			solucion.remove_at(solucion.index_of(c.Ref))
		c.Fondo_Visible=false
		organiza_iman()
		
		
	def on_soltar_letra(c:Control)
		if sdlk.colision_mascara(iman,c) 
			c.Valor_bool=true
			c.Fondo_Visible=false
			solucion.add(c.Ref)
			c.set_Posicion_y((iman.get_Posicion_y()+iman.get_Alto()/2)-(c.get_Alto()/2))
			sonidos.play("blub")
		else
			c.Fondo_Visible=true
			c.Valor_bool=false
			if solucion.contains(c.Ref)
				solucion.remove_at(solucion.index_of(c.Ref))
		organiza_iman()
		corrige();
		
	def on_salir()
		fin=true
		num_ejercicio=-2
	
		
	def organiza_iman()
		var pos=15+iman.get_Posicion_x()
		for var i=0 to ultimo_de_lista(solucion)
			if letras[solucion[i]].Valor_bool
				letras[solucion[i]].set_Posicion_x(pos)
				letras[solucion[i]].Fondo_Visible=false
				pos+=letras[solucion[i]].get_Ancho()-40
		
		

	def corrige()
		var cadena_solucion=""
		for var i=0 to ultimo_de_lista(solucion)
			cadena_solucion+=letras[solucion[i]].get_Texto()
		if cadena_solucion==palabra_ordenada
			sonidos.play("ondo")
			fin=true
			num_ejercicio=0
			
	def pinta_fondo_color()
		//sdlk.Rectangulo_xywh( screen, 0, 0, sdlk.Ancho, sdlk.Alto, 255,210,151,255) //pintando fondo
		screen.copy (wall2, null , {0,0,sdlk.Ancho,sdlk.Alto});

